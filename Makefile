all: help

.PHONY: help
help:
	@echo make targets:
	@echo     - test
	@echo     - clean
	@echo     - release: build standalone binaries

.PHONY: test
test: unit reuse

.PHONY: unit
unit:
	cargo test

.PHONY: reuse
reuse:
	docker run --rm -v $(PWD):/bitwise fsfe/reuse /bin/sh -c "cd /bitwise && reuse lint"

.PHONY: clean
clean:
	rm -rf target || true
	rm bitwise_linux_amd64 || true

.PHONY: release
release: bitwise_linux_amd64

bitwise_linux_amd64:
	rustup target add x86_64-unknown-linux-musl
	cargo build --release --target x86_64-unknown-linux-musl
	strip target/x86_64-unknown-linux-musl/release/bitwise
	cp target/x86_64-unknown-linux-musl/release/bitwise bitwise_linux_amd64
