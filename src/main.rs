/*  bitwise: perform binary operations on data.
    Copyright (C) 2020  Jérôme Jutteau <jerome sign-A-T jutteau sign-D-O-T fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::env;
use std::error::Error;
use std::fmt;
use std::fs::OpenOptions;
use std::io;
use std::process::exit;

const DEFAULT_BUF_SIZE: usize = 1024;
const VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn main() {
    let bitwise = match Bitwise::new_from_args() {
        Ok(b) => b,
        Err(error) => {
            eprintln!("error: {}", error);
            eprintln!("use -h or --help for more information");
            exit(1);
        }
    };
    if let Err(error) = bitwise.run() {
        eprintln!("error: {}", error);
        exit(1);
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Operation {
    Xor,
    Or,
    And,
    Not,
}

impl Operation {
    fn takes(&self) -> usize {
        match self {
            Operation::Xor => 2,
            Operation::Or => 2,
            Operation::And => 2,
            Operation::Not => 1,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
enum Reader {
    Stdin,
    InputFile(String),
}

impl Reader {
    fn open(&self) -> Result<Box<dyn io::Read>, io::Error> {
        match self {
            Reader::Stdin => Ok(Box::new(io::stdin())),
            Reader::InputFile(path) => match OpenOptions::new().read(true).open(path) {
                Ok(file) => Ok(Box::new(file)),
                Err(e) => Err(e),
            },
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
enum Writer {
    Stdout,
    OutputFile(String),
}

impl Writer {
    fn open(&self) -> Result<Box<dyn io::Write>, io::Error> {
        match self {
            Writer::Stdout => Ok(Box::new(io::stdout())),
            Writer::OutputFile(path) => {
                match OpenOptions::new().create(true).write(true).open(path) {
                    Ok(file) => Ok(Box::new(file)),
                    Err(e) => Err(e),
                }
            }
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Padding {
    Byte(u8),
}

impl Padding {
    fn write(self, out: &mut [u8]) {
        match self {
            Padding::Byte(b) => {
                for o in out.iter_mut() {
                    *o = b;
                }
            }
        };
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Conf {
    help: bool,
    version: bool,
    op: Operation,
    inputs: Vec<Reader>,
    output: Writer,
    pad: Option<Padding>,
    buf_size: usize,
}

impl Conf {
    fn new() -> Conf {
        Conf {
            help: false,
            version: false,
            op: Operation::Xor,
            inputs: Vec::new(),
            output: Writer::Stdout,
            pad: None,
            buf_size: DEFAULT_BUF_SIZE,
        }
    }

    fn from_args() -> Result<Conf, ConfError> {
        let args: Vec<String> = env::args().collect();
        return Conf::parse_args(&args);
    }

    fn parse_args(args: &Vec<String>) -> Result<Conf, ConfError> {
        let mut conf = Conf::new();
        for arg in args.iter().skip(1) {
            match arg.as_str() {
                "-h" | "--help" => {
                    conf.help = true;
                    return conf.check();
                }
                _ => {}
            }
        }

        for arg in args.iter().skip(1) {
            match arg.as_str() {
                "--version" => {
                    conf.version = true;
                    return conf.check();
                }
                _ => {}
            }
        }

        let mut iter = args.iter().skip(1);
        match iter.next() {
            Some(op) => match op.as_str() {
                "xor" => conf.op = Operation::Xor,
                "or" => conf.op = Operation::Or,
                "and" => conf.op = Operation::And,
                "not" => conf.op = Operation::Not,
                _ => return Err(ConfError::InvalidOp),
            },
            None => return Err(ConfError::NoArgProvided),
        };

        while let Some(arg) = iter.next() {
            match arg.as_str() {
                "-o" | "--output" => match iter.next() {
                    Some(path) => conf.output = Writer::OutputFile(path.to_string()),
                    None => return Err(ConfError::MissingArg("--output".to_string())),
                },
                "-b" | "--pad-byte" => match iter.next() {
                    Some(hex_byte_str) => match u8::from_str_radix(hex_byte_str, 16) {
                        Ok(b) => conf.pad = Some(Padding::Byte(b)),
                        Err(_) => return Err(ConfError::HexConvert(hex_byte_str.to_string())),
                    },
                    None => return Err(ConfError::MissingArg("--pad-byte".to_string())),
                },
                "--buffer-size" => match iter.next() {
                    Some(size_str) => match usize::from_str_radix(size_str, 10) {
                        Ok(s) if s > 0 => conf.buf_size = s,
                        _ => return Err(ConfError::UintConvert(size_str.to_string())),
                    },
                    None => return Err(ConfError::MissingArg("--buffer-size".to_string())),
                },
                _path => conf.inputs.push(Reader::InputFile(_path.to_string())),
            }
        }
        if conf.inputs.len() + 1 == conf.op.takes() {
            conf.inputs.push(Reader::Stdin);
        }
        return conf.check();
    }

    fn check(self) -> Result<Self, ConfError> {
        if self.help == true {
            return Ok(self);
        }
        if self.version == true {
            return Ok(self);
        }
        if self.op.takes() != self.inputs.len() {
            return Err(ConfError::InputCount(self.op.takes(), self.inputs.len()));
        }
        Ok(self)
    }
}

#[derive(Debug, PartialEq)]
enum ConfError {
    NoArgProvided,
    MissingArg(String),
    HexConvert(String),
    UintConvert(String),
    InputCount(usize, usize),
    InvalidOp,
}

impl Error for ConfError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl fmt::Display for ConfError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ConfError::NoArgProvided => write!(f, "no argument provided"),
            ConfError::MissingArg(arg) => write!(f, "missing argument for option {}", arg),
            ConfError::HexConvert(s) => write!(f, "cannot convert {} to hexadecimal", s),
            ConfError::UintConvert(s) => write!(f, "cannot convert {} to positive integer", s),
            ConfError::InputCount(needed, provided) => {
                write!(f, "operation need {} inputs, {} provided", needed, provided)
            }
            ConfError::InvalidOp => write!(f, "unknown operation"),
        }
    }
}

struct Bitwise {
    conf: Conf,
}

impl Bitwise {
    fn new_from_args() -> Result<Bitwise, BitwiseError> {
        let conf = match Conf::from_args() {
            Ok(c) => c,
            Err(error) => return Err(BitwiseError::Conf(error)),
        };
        Ok(Bitwise { conf: conf })
    }

    fn run(&self) -> Result<(), BitwiseError> {
        if self.conf.help {
            print!("{}", include_str!("help.txt"));
            return Ok(());
        }
        if self.conf.version {
            println!("bitwise version {}", VERSION);
            return Ok(());
        }
        let mut it = self.conf.inputs.iter();
        let mut input0 = match it.next() {
            Some(input) => match input.open() {
                Ok(i) => i,
                Err(e) => return Err(BitwiseError::OpenInput(e)),
            },
            None => return Err(BitwiseError::Bug(std::file!(), std::line!())),
        };

        let mut output = match self.conf.output.open() {
            Ok(o) => o,
            Err(e) => return Err(BitwiseError::OpenOutput(e)),
        };

        if let Some(input1) = it.next() {
            let mut in1 = match input1.open() {
                Ok(i) => i,
                Err(e) => return Err(BitwiseError::OpenInput(e)),
            };
            return self.write_loop(&mut input0, Some(&mut in1), &mut output);
        } else {
            return self.write_loop(&mut input0, None, &mut output);
        }
    }

    fn write_loop<R: io::Read, W: io::Write>(
        &self,
        input0: &mut R,
        mut input1: Option<&mut R>,
        output: &mut W,
    ) -> Result<(), BitwiseError> {
        let mut buff0 = vec![0u8; self.conf.buf_size];
        let mut buff1 = vec![0u8; self.conf.buf_size];
        loop {
            // read first buffer
            let read_size0 = match input0.read(&mut buff0.as_mut_slice()) {
                Ok(size) => size,
                Err(e) => return Err(BitwiseError::Read(e)),
            };

            // read second buffer
            let read_size1 = match &mut input1 {
                Some(in1) => match in1.read(&mut buff1.as_mut_slice()) {
                    Ok(size) => Some(size),
                    Err(e) => return Err(BitwiseError::Read(e)),
                },
                None => None,
            };

            // end processing or possibly write padding
            let write_size = match (&self.conf.pad, read_size0, read_size1) {
                (_, 0, None) => break,
                (_, 0, Some(0)) => break,
                (None, r0, Some(r1)) => r0.min(r1),
                (None, r0, None) => r0,
                (Some(pad), r0, Some(r1)) if r0 < r1 => {
                    pad.write(&mut buff0[r0..r1]);
                    r1
                }
                (Some(pad), r0, Some(r1)) if r0 > r1 => {
                    pad.write(&mut buff1[r1..r0]);
                    r0
                }
                (_, r0, _) => r0,
            };

            // bitwise operation in first buffer and write result
            self.operation(&mut buff0[..write_size], &buff1[..write_size]);
            if let Err(e) = output.write(&buff0[..write_size]) {
                return Err(BitwiseError::Write(e));
            }
        }
        return Ok(());
    }

    fn operation(&self, mut in0: &mut [u8], in1: &[u8]) {
        match self.conf.op {
            Operation::Xor => Bitwise::xor(&mut in0, &in1),
            Operation::Or => Bitwise::or(&mut in0, &in1),
            Operation::And => Bitwise::and(&mut in0, &in1),
            Operation::Not => Bitwise::not(&mut in0),
        }
    }

    fn xor(in0: &mut [u8], in1: &[u8]) {
        for i in 0..in0.len() {
            in0[i] ^= in1[i];
        }
    }

    fn or(in0: &mut [u8], in1: &[u8]) {
        for i in 0..in0.len() {
            in0[i] |= in1[i];
        }
    }

    fn and(in0: &mut [u8], in1: &[u8]) {
        for i in 0..in0.len() {
            in0[i] &= in1[i];
        }
    }

    fn not(in0: &mut [u8]) {
        for i in 0..in0.len() {
            in0[i] = !in0[i];
        }
    }
}

#[derive(Debug)]
enum BitwiseError {
    Conf(ConfError),
    OpenInput(io::Error),
    OpenOutput(io::Error),
    Read(io::Error),
    Write(io::Error),
    Bug(&'static str, u32),
}

impl Error for BitwiseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl fmt::Display for BitwiseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            BitwiseError::Conf(conf_error) => write!(f, "{}", conf_error),
            BitwiseError::OpenInput(e) => write!(f, "cannot open input, {}", e),
            BitwiseError::OpenOutput(e) => write!(f, "cannot open output, {}", e),
            BitwiseError::Read(e) => write!(f, "cannot read input, {}", e),
            BitwiseError::Write(e) => write!(f, "canot write output, {}", e),
            BitwiseError::Bug(path, line) => write!(
                f,
                "bitwise {} bug in file {} line {}, please report this issue",
                VERSION, path, line
            ),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    fn vector1_test(conf: &Conf, mut input0: &'static [u8], expected: &[u8]) {
        let mut buff = vec![42u8; expected.len()];
        let mut curs = Cursor::new(&mut buff);
        let bitwise = Bitwise { conf: conf.clone() };
        if let Err(e) = bitwise.write_loop(&mut input0, None, &mut curs) {
            assert!(true, "{}", e);
        }
        assert_eq!(expected, buff.as_slice());
    }

    fn vector2_test_mirror(
        conf: &Conf,
        mut input0: &'static [u8],
        mut input1: &'static [u8],
        expected: &[u8],
    ) {
        vector2_test(&conf, &mut input0, &mut input1, &expected);
        vector2_test(&conf, &mut input1, &mut input0, &expected);
    }

    fn vector2_test(
        conf: &Conf,
        mut input0: &'static [u8],
        mut input1: &'static [u8],
        expected: &[u8],
    ) {
        let mut buff = vec![51u8; expected.len()];
        let mut curs_out = Cursor::new(&mut buff);
        let mut curs0 = Cursor::new(&mut input0);
        let mut curs1 = Cursor::new(&mut input1);
        let bitwise = Bitwise { conf: conf.clone() };
        if let Err(e) = bitwise.write_loop(&mut curs0, Some(&mut curs1), &mut curs_out) {
            assert!(true, "{}", e);
        }
        assert_eq!(expected, buff.as_slice());
    }

    #[test]
    fn minimal_buffer_size_1byte() {
        let mut conf = Conf::new();
        conf.buf_size = 1;
        conf.op = Operation::Xor;
        vector2_test_mirror(&conf, &[0x00], &[0xf0], &[0xf0]);
        vector2_test_mirror(
            &conf,
            &[0x00, 0xf0, 0xff],
            &[0xf0, 0xff, 0x00],
            &[0xf0, 0x0f, 0xff],
        );
    }

    #[test]
    fn non_perfect_buffer_size() {
        let mut conf = Conf::new();
        conf.buf_size = 2;
        conf.op = Operation::Xor;
        vector2_test_mirror(
            &conf,
            &[0x00, 0xf0, 0xff],
            &[0xf0, 0xff, 0x00],
            &[0xf0, 0x0f, 0xff],
        );
    }

    #[test]
    fn smaller_than_buffer() {
        let mut conf = Conf::new();
        conf.buf_size = 4;
        conf.op = Operation::Xor;
        vector2_test_mirror(
            &conf,
            &[0x00, 0xf0, 0xff],
            &[0xf0, 0xff, 0x00],
            &[0xf0, 0x0f, 0xff],
        );
    }

    #[test]
    fn xor_logic() {
        let mut conf = Conf::new();
        conf.op = Operation::Xor;
        vector2_test_mirror(&conf, &[0b00111100], &[0b01011010], &[0b01100110]);
    }

    #[test]
    fn or_logic() {
        let mut conf = Conf::new();
        conf.op = Operation::Or;
        vector2_test_mirror(&conf, &[0b00111100], &[0b01011010], &[0b01111110]);
    }

    #[test]
    fn and_logic() {
        let mut conf = Conf::new();
        conf.op = Operation::And;
        vector2_test_mirror(&conf, &[0b00111100], &[0b01011010], &[0b00011000]);
    }

    #[test]
    fn not_logic() {
        let mut conf = Conf::new();
        conf.op = Operation::Not;
        vector1_test(&conf, &[0b10101010], &[0b01010101]);
    }

    #[test]
    fn padding_none() {
        let mut conf = Conf::new();
        conf.op = Operation::Or;
        vector2_test_mirror(
            &conf,
            &[0x00, 0x00, 0x00, 0x42, 0x42],
            &[0xff, 0xff, 0xff],
            &[0xff, 0xff, 0xff],
        );
    }

    #[test]
    fn padding_byte() {
        let mut conf = Conf::new();
        conf.op = Operation::Or;
        conf.pad = Some(Padding::Byte(0x00));
        vector2_test_mirror(
            &conf,
            &[0x12, 0x31, 0x42, 0x51],
            &[0x00, 0x00],
            &[0x12, 0x31, 0x42, 0x51],
        );
    }

    #[test]
    fn padding_byte_empty_input() {
        let mut conf = Conf::new();
        conf.op = Operation::Or;
        conf.pad = Some(Padding::Byte(0x00));
        vector2_test_mirror(
            &conf,
            &[0x12, 0x31, 0x42, 0x51],
            &[],
            &[0x12, 0x31, 0x42, 0x51],
        );
        conf.pad = Some(Padding::Byte(0xff));
        vector2_test_mirror(
            &conf,
            &[0x12, 0x31, 0x42, 0x51],
            &[],
            &[0xff, 0xff, 0xff, 0xff],
        );
    }

    #[test]
    fn no_arg() {
        let args = vec!["bitwise".to_string()];
        assert_eq!(Conf::parse_args(&args), Err(ConfError::NoArgProvided));
    }

    #[test]
    fn help() {
        let args = vec!["bitwise".to_string(), "--help".to_string()];
        assert!(Conf::parse_args(&args).is_ok());
        let args = vec![
            "bitwise".to_string(),
            "xor".to_string(),
            "--help".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
        let args = vec![
            "bitwise".to_string(),
            "xo".to_string(),
            "--help".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
        let args = vec![
            "bitwise".to_string(),
            "--version".to_string(),
            "--help".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
    }

    #[test]
    fn version() {
        let args = vec!["bitwise".to_string(), "--version".to_string()];
        assert!(Conf::parse_args(&args).is_ok());
        let args = vec![
            "bitwise".to_string(),
            "xor".to_string(),
            "--version".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
        let args = vec![
            "bitwise".to_string(),
            "xo".to_string(),
            "--version".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
    }

    #[test]
    fn invalid_op() {
        let args = vec!["bitwise".to_string(), "xo".to_string()];
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InvalidOp));
    }

    #[test]
    fn xor_bad_number_of_inputs() {
        let mut args = vec!["bitwise".to_string(), "xor".to_string()];
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(2, 0)));
        args.push("f1".to_string());
        args.push("f2".to_string());
        args.push("f3".to_string());
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(2, 3)));
    }

    #[test]
    fn or_bad_number_of_inputs() {
        let mut args = vec!["bitwise".to_string(), "or".to_string()];
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(2, 0)));
        args.push("f1".to_string());
        args.push("f2".to_string());
        args.push("f3".to_string());
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(2, 3)));
    }

    #[test]
    fn and_bad_number_of_inputs() {
        let mut args = vec!["bitwise".to_string(), "and".to_string()];
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(2, 0)));
        args.push("f1".to_string());
        args.push("f2".to_string());
        args.push("f3".to_string());
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(2, 3)));
    }

    #[test]
    fn not_bad_number_of_inputs() {
        let args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "f1".to_string(),
            "f2".to_string(),
        ];
        assert_eq!(Conf::parse_args(&args), Err(ConfError::InputCount(1, 2)));
    }

    #[test]
    fn output_missing_arg() {
        let mut args = vec!["bitwise".to_string(), "not".to_string(), "-o".to_string()];
        assert_eq!(
            Conf::parse_args(&args),
            Err(ConfError::MissingArg("--output".to_string()))
        );
        args.pop();
        args.push("--output".to_string());
        assert_eq!(
            Conf::parse_args(&args),
            Err(ConfError::MissingArg("--output".to_string()))
        );
    }

    #[test]
    fn output_file() {
        let args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "-o".to_string(),
            "out".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
        let args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--output".to_string(),
            "out".to_string(),
        ];
        assert!(Conf::parse_args(&args).is_ok());
    }

    #[test]
    fn buffer_size_missing_arg() {
        let args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--buffer-size".to_string(),
        ];
        assert_eq!(
            Conf::parse_args(&args),
            Err(ConfError::MissingArg("--buffer-size".to_string()))
        );
    }

    #[test]
    fn buffer_size_bad_int() {
        let bad_int = vec![
            "0".to_string(),
            "ff".to_string(),
            "-1".to_string(),
            "99999999999999999999999999999".to_string(),
        ];
        let mut args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--buffer-size".to_string(),
        ];
        for b in bad_int.iter() {
            args.push(b.clone());
            assert_eq!(
                Conf::parse_args(&args),
                Err(ConfError::UintConvert(b.clone()))
            );
            args.pop();
        }
    }

    #[test]
    fn buffer_size_good_int() {
        let bad_int = vec!["1".to_string(), "1000000000000".to_string()];
        let mut args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--buffer-size".to_string(),
        ];
        for b in bad_int.iter() {
            args.push(b.clone());
            assert!(Conf::parse_args(&args).is_ok());
            args.pop();
        }
    }

    #[test]
    fn pad_byte_missing_arg() {
        let args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--pad-byte".to_string(),
        ];
        assert_eq!(
            Conf::parse_args(&args),
            Err(ConfError::MissingArg("--pad-byte".to_string()))
        );
    }

    #[test]
    fn pad_byte_bad_hex() {
        let bad_hex = vec![
            "-1".to_string(),
            "ff1".to_string(),
            "0x42".to_string(),
            "0xg".to_string(),
            "9xaa".to_string(),
            "0xff1".to_string(),
            "0xff0".to_string(),
        ];
        let mut args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--pad-byte".to_string(),
        ];
        for b in bad_hex.iter() {
            args.push(b.clone());
            assert_eq!(
                Conf::parse_args(&args),
                Err(ConfError::HexConvert(b.clone()))
            );
            args.pop();
        }
    }

    #[test]
    fn pad_byte_good_hex() {
        let bad_hex = vec![
            "0".to_string(),
            "0ff".to_string(),
            "000".to_string(),
            "ff".to_string(),
        ];
        let mut args = vec![
            "bitwise".to_string(),
            "not".to_string(),
            "--pad-byte".to_string(),
        ];
        for b in bad_hex.iter() {
            args.push(b.clone());
            let result = Conf::parse_args(&args);
            eprintln!("{:?}", result);
            assert!(result.is_ok());
            args.pop();
        }
    }
}
