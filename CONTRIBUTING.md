# Reporting a bug or suggesting an enhancement

Feel free to open an issue!

Please provide any relevent details in case of bug like:
- Your OS and version/flavor
- Bitwise version (`bitwise --version`)
- How to reproduce the bug

# Tooling

Some tools used in this project:
- [GNU Make](https://www.gnu.org/software/make/)
- [rust](https://www.rust-lang.org) (stable channel)
- [docker](https://www.docker.com/) (only used to run reuse)

# Building

Usual Rust command: `cargo build`.

Run `make release` to build standalone binaries.

# Testing

Just run `make test` to run all tests. Those includes:
- unit tests
- reuse specification check

# Submitting change

- Before any merge request, please open an issue
- Make sure tests still pass
- You may need to add more tests
- Rust guidelines:
  - no unwrap(), no panic()
  - std only
  - keep it simple stupid
