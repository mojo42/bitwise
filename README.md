# Bitwise

`bitwise` is a command line tool to make binary operations on data.

# Features

- Perform bitwise operations:
  - xor
  - or
  - and
  - not
- Pad with specific value

See [`bitwise --help`](./src/help.txt) for more details.

# Download

Check [release page](https://gitlab.com/mojo42/bitwise/-/releases)

# Usage example

Encrypt and decrypt a message through a [One-time-pad](https://en.wikipedia.org/wiki/One-time_pad):
```bash
# create message
echo "hello, world" > plain.txt
# create one-time pad
dd if=/dev/urandom of=pad.key bs=1 count=$(cat plain.txt | wc -c)
# encrypt
cat plain.txt | bitwise xor pad.key > ciphertext
# decrypt
cat ciphertext | bitwise xor pad.key > decrypted.txt
```

# Suggestion? Bug?

Feel free to open an issue. For more information, see how to [contribute](./CONTRIBUTING.md).

# License

`bitwise` is licensed under:
- [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0.html) (GPL-3.0-or-later) and
- [GNU Free Documentation License v1.3 or later](https://www.gnu.org/licenses/fdl-1.3.html) (GFDL-1.3-or-later)

This project is compliant version 3.0 of the [REUSE](https://reuse.software/) Specification.
